# Find an approximate solution to a function using the Newton Raphson method.

# The function, f, we are solving
poly5 = lambda x: x**5 - 5*x + 1

# Its gradient (needed for Newton Raphson iterations)
dPoly5dx = lambda x: 5*x**4 - 5

# The Root finding algorithm

def solveNewtonRaphson(f, dfdx, a, nmax=100, e=1e-6):
    """Solve f by Newton Raphson iterations starting from a.
    dfdx is the function for the gradient of f.
    Optional argument nmax is the maximum number of iterations
    Optional argument e is the tolerance for finding a root
    solveNewtonRaphson returns the root and the number of iterations used"""
    
    # Iterate until the solution is within the error or too many iterations
    for it in range(nMax):
        a -= f(a)/dfdx(a)
        if abs(f(a)) < e:
            break
    else:
        raise Exception("No root found by solveNewtonRaphson")
    return a, it

nMax = 100

[root, nIt] = solveNewtonRaphson(poly5, dPoly5dx, 1, nmax=nMax)
print("First root found is ", root,  " in ", nIt, " iterations")

[root, nIt] = solveNewtonRaphson(poly5, dPoly5dx, 101.5, nmax=10)
print("Second root found is ", root, " in ", nIt, " iterations")

# Checking
print("poly5(", root, ") = ", poly5(root))
